import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Button, Row, Col, } from 'react-bootstrap';
import './style.css';


const ScreenSettings = (props) => {
  const [mode, setMode] = useState('serial');
  const [duration, setDuration] = useState();
  const modeChangeHandler = (event) => {
    switch (event.target.value) {
      case 'serial':
        setMode('serial');
        break;
      case 'opposite':
        setMode('opposite');
        break;
      case 'right':
        setMode('right');
        break;
      default:
        setMode('serial');
    }
  };

  const saveButtonClickHandler = () => {
    props.setMode({
      mode,
      duration,
    });
    props.changeScreen('crossroad');
  };

  return (
    <>

      <Container>

        <Row>
          <Col>
            <h1 className='header'>Настройте работу светофора на перекрестке</h1>
            <p className='Explanation'>
              Для настройки работы светофора выберите режим и укажите
              продолжительность фазы в секундах:
            </p>

          </Col>
        </Row>
        <Col>
          <p className='mode'>Режимы работы светофора:</p>
        </Col>
        <Col>

          <label>
            <input
              id='toolTip'
              data-toggle='tooltip'
              title='На каждой фазе разрешено движение только по одному направлению. Последовательно разрешается 4-м направлениям.'
              type='radio'
              name='mode'
              value='serial'
              checked={mode === 'serial'}
              onChange={modeChangeHandler}
            />{' '}
            Последовательный
          </label>
        </Col>
        <br />
        <Col>
          <label>
            <input
              id='toolTip'
              data-toggle='tooltip'
              title='На каждой фазе разрешено движение полос противоположных направлений.'
              type='radio'
              name='mode'
              value='opposite'
              checked={mode === 'opposite'}
              onChange={modeChangeHandler}
            />{' '}
            Противоположный
          </label>
        </Col>
        <br />
        <Col>
          <label>
            <input
              id='toolTip'
              data-toggle='tooltip'
              title='Аналогичен режиму "Противоположный", но при запрещающем сигнале светофора в прямом направлении будет разрешен поворот направо (дополнительная секция).'
              type='radio'
              name='mode'
              value='right'
              checked={mode === 'right'}
              onChange={modeChangeHandler}
            />{' '}
            C поворотом направо
          </label>
        </Col>
        <br />
        <Col>

          <input
            type='number'
            placeholder='Введите секунды'
            id='seconds'
            min='0'
            step='1'
            value={duration}
            onInput={(e) => {
              setDuration(e.target.value);
            }}
          />

        </Col>
        <br />
        <Col>
          <Button variant="primary" onClick={saveButtonClickHandler} type='button' id='save'>Сохранить</Button>


        </Col>

      </Container>

    </>
  );
};

export default ScreenSettings;
